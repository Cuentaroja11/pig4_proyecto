import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';


let linkbackend="http://127.0.0.1:4000/";
let vez=1;

export default class Cliente extends React.Component {
    //getClientes();
    state = {
        data: [],
        sedes:[],
        form: {
            codigocliente:'',
            nombre: '',
            nit: '',
            dpi: '',
            direccion: '',
            codigosede: ''
        },
        modalInsertar: false,
        modalEditar: false
    }

    getClientes = () => {
        const url=linkbackend+"getClients";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ data: data }));
        console.log(this.state.data);

    }

    getSedes = () => {
        const url=linkbackend+"getsedes";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ sedes: data }));
        console.log(this.state.data);
    }


    insertar = () => {
        const url=linkbackend+"insertClient";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registro guardado!!");
                }
               this.hideModalInsertar();
              this.getClientes();
            })
    }


    editar = (Cliente) => {
        const url=linkbackend+"updateClient";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo actualizar el registro");
                }else{
                    alert("Registro actualizado!!");
                }
               this.hideModalEditar();
              this.getClientes();
            })
    }

    eliminar = (Cliente) => {
        var opcion=window.confirm("Desea eliminar el cliente?");
        const url=linkbackend+"deleteClient";
        const json="{ \"codigocliente\":"+Cliente.codigocliente+"}";
        console.log(json);
        if(opcion){
            fetch(url, {
                method: 'DELETE',
                body: json,
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }
              })
                .then(res => res.json())
                .then(data => {
                    if(data.status==false){
                        alert("No se pudo eliminar el registro");
                    }else{
                        alert("Registro eliminado!!");
                    }
                
                    this.getClientes();
                });
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true, form: registro });
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }
    render() {
        if(vez==1){
            this.getClientes();
            this.getSedes();
            vez=2;
        }
        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success" onClick={() => this.showModalInsertar()}>
                            Agregar nuevo Cliente
                     </Button>

                    </nav>
                    <br />
                    <Table dark>
                        <thead>

                        </thead>

                        <th>  nombre Cliente </th>
                        <th>  nit</th>
                        <th>  dpi</th>
                        <th>  direccion</th>
                        <th>  Opciones</th>

                        <tbody>
                            {this.state.data.map((Cliente) => (
                                <tr>
                                    <td> {Cliente.nombre}</td>
                                    <td> {Cliente.nit}</td>
                                    <td> {Cliente.dpi}</td>
                                    <td> {Cliente.direccion}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(Cliente)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar(Cliente)}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Cliente</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                nombre Cliente:
                             </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                nit:
                            </label>
                            <input
                                className="form-control"
                                name="nit"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                dpi:
                            </label>
                            <input
                                className="form-control"
                                name="dpi"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                direccion:
                            </label>
                            <input
                                className="form-control"
                                name="direccion"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                Sede:
                            </label>
                            <select className="form-control" name="codigosede" onChange={this.handleChange}>
                            <option value={-1}> </option>
                                {this.state.sedes.map((sede) => (
                                    <option value={sede.codigosede}>{sede.aliassede}</option>

                                ))}

                            </select>

                        </FormGroup>

                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Cliente</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                nombre Cliente:
                            </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                value={this.state.form.nombre}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                nit:
                             </label>
                            <input
                                className="form-control"
                                name="nit"
                                type="text"
                                value={this.state.form.nit}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                dpi:
                             </label>
                            <input
                                className="form-control"
                                name="dpi"
                                type="text"
                                value={this.state.form.dpi}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                direccion:
                            </label>
                            <input
                                className="form-control"
                                name="direccion"
                                type="text"
                                value={this.state.form.direccion}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Sede:
                            </label>
                            <select className="form-control" name="codigosede" onChange={this.handleChange}>
                            <option value={-1}> </option>
                                {this.state.sedes.map((sede) => (
                                    <option value={sede.codigosede}>{sede.aliassede}</option>

                                ))}

                            </select>

                        </FormGroup>
                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

