import React, { Component,useState,useEffect } from 'react'

import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import {  Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import {Tabs, Tab} from 'react-bootstrap-tabs';
import { DateRange, DateRangePicker } from 'react-date-range';
import {Line,Bar,Pie} from 'react-chartjs-2';
var dummyData = {
    "jsonarray":[
        {
            "venta":10,
            "fecha":202000000
        },{
            "venta":50,
            "fecha":202000000
        },{
            "venta":20,
            "fecha":201000000
        }
    ]
}
export default class VentasLineal extends Component {

    componentDidMount(){
        var chart =this.chart;
        /*fetch('uri').then(function(response){
            return response.json();
        }).then(function(data){
            for(let i = 0;i<data.length;i++){
                
            }
        })*/
    }
    handleSelect(ranges){
        console.log(ranges);
    }
    render() {
        const selectionRange={
            startDate:new Date(),
            endDate:new Date(),
            key:'selection',
        }
        const rangoTiempo=[
            { value: '0', label: 'Día' },
            { value: '1', label: 'Semana' },
            { value: '2', label: 'Mes' },
        ]
        const fixedUsers=[
            [
                {value:'2929292',label:'Alvaro Pepe'},
                {value:'29292',label:'Shun Pepe'},
                {value:'9292',label:'Ney Pepe'},
            ]
        ]
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <label>Agrupar</label>                                
                        <select options={rangoTiempo}></select>
                    </div>
                    <div className="col-sm">
                        <label>Usuarios</label>                                
                        <select options={fixedUsers}></select>
                    </div>
                    <div className="col-sm">        
                        <label>Rango de días </label>
                        <DateRangePicker
                            ranges={[selectionRange]}
                            onChange={this.handleSelect}
                        />
                    </div>
                </div>
                <div className="row ">
                    <div className="col">
                    <Tabs>
                        <Tab label="Linea">
                            Linea
                        </Tab>
                        <Tab label="Barra">
                            Barra
                        </Tab>
                        <Tab label="Pie">
                            Pie
                        </Tab>
                    </Tabs>
                    </div>
                </div>
                
            </div>
        )
    }
}
