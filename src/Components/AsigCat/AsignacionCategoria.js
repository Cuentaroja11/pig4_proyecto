import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';



let linkbackend = "http://127.0.0.1:4000/";
export default class AsignacionCat extends React.Component {

    state = {
        data: [],
        catsasig: [],
        form: {
            codigoproducto: this.props.id,
            codigocategoria: ''
        },
        modalInsertar: false,
        modalEditar: false
    }

    getcatsProducto = () => {

        const url=linkbackend+"getCategoryProduct/"+this.props.id;
        console.log(url);
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ catsasig: data }));
       // console.log(this.state.data);

    }

    getCats = () => {
        const url = linkbackend + "categories";
        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({ data: data }));
        // console.log(this.state.data);

    }




    insertar = () => {
        const url = linkbackend + "assignCategory";
        var valorNuevo = { ...this.state.form };
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data.status == false) {
                    alert("No se pudo asignar el registro");
                } else {
                    alert("Registor asignado!!");
                    this.getcatsProducto();
                }
                this.hideModalInsertar();

            })
    }


    eliminar = (categoria) => {
        var opcion = window.confirm("Desea eliminar el registro?");
        const url = linkbackend + "deleteAssignCategory";
        const jeison="{ \"codigoproducto\":"+this.props.id+",\n \"codigocategoria\":"+categoria.codigocategoria+"}";
        console.log(jeison);
        if (opcion) {
            fetch(url, {
                method: 'DELETE',
                body: jeison,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    if (data.status == false) {
                        alert("No se pudo eliminar el registro");
                    } else {
                        alert("Registro eliminado!!");
                    }



                    this.getcatsProducto();
                });
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });

    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    render() {


        this.getCats();
        this.getcatsProducto();

        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success" onClick={() => this.showModalInsertar()}>
                            Agregar una categoria al producto {this.props.nombre}
                        </Button>

                    </nav>
                    <br />
                    <Table dark>
                        <thead>

                        </thead>

                        <th>  Categorias Agregadas </th>
                        <th>  Opciones </th>

                        <tbody>
                            {this.state.catsasig.map((categoria) => (
                                <tr>
                                    <td> {categoria.nombrecategoria}</td>
                                    <td> <Button color="danger" onClick={() => this.eliminar(categoria)}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar categoria</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Categorias:
                            </label>
                            <select className="form-control" name="codigocategoria" onChange={this.handleChange}>
                                <option value={-1}> </option>
                                {this.state.data.map((categoria) => (
                                    <option value={categoria.codigocategoria}>{categoria.nombrecategoria}</option>
                                ))}

                            </select>
                        </FormGroup>

                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Asignar categoria
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

