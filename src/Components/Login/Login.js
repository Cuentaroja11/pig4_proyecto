import React, { Component } from 'react'
import { FormGroup } from 'reactstrap';
import HomeVen from '../Home/HomeVen';
import HomeBod from '../Home/HomeBod';
import HomeAdmin from '../Home/HomeAdmin';
import LogOut from '../Logout/Logout';

const API_URI = 'http://localhost:4000/'

export default class Login extends Component {
    
    state={
        form:{
            correo:'',
            contraseña:'',
        },
        usuario:{
            Codigousuario:'',
            dpi:'',
            nombre:'',
            fechanac:'',
            correo:'',
            contraseña:'',
            roles:''
        },
        redirect: false
    }

    setRedirect = () => {
        this.setState({
          redirect: true
        })
      }

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }
    login(){
        //let login = {...this.state.form};
        //var lista = this.state.data;
        //lista.push(login);
        //;
        const url = API_URI+'login';
        var valorNuevo = {...this.state.form}
        //alert(valorNuevo.contraseña)
        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
          .then(res => res.json())
          .then(data => {
              if(data.Codigousuario===undefined){
                  alert("Contraseña o correo incorrectos");
                  console.log(data);
              }else{
                //Loggearse
                this.setState({usuario:data});
                alert({usuario:data});
                //
                localStorage.setItem('Codigousuario',this.state.usuario.Codigousuario);
                const url = API_URI+'getUsuario/'+this.state.usuario.Codigousuario;
                fetch(url, {
                    method: 'GET',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    }
                  })
                .then(res=>res.json())
                .then(user=>{
                    this.setState({usuario:user});
                    const roles = this.state.usuario.codigorol.split(',');
                    
                    roles.map((m=>{
                        if(m===1){
                            localStorage.setItem('vendedor',true);
                        }else if(m==2){
                            localStorage.setItem('bodeguero',true);
                        }
                        else if(m==3){
                            localStorage.setItem('repartidor',true);
                        }
                        else if(m==0){
                            localStorage.setItem('admin',true);
                        }
                    }));
                    localStorage.setItem('loggeado',true);
                    window.location.reload();
                });    

            }
            });
    
    }    

    getUsuarios(){
        const url = API_URI+'getUsuarios'
        fetch(url)
        .then(res=>res.json())
        .then(usuarios=>{
            this.setState({data:usuarios});
            
        console.log(this.state.data);
        })
    }


    render() {
        //localStorage.setItem('loggeado',true);
        if (localStorage.getItem('loggeado')==='true') {
            //localStorage.setItem('bodeguero',false);
            //localStorage.setItem('vendedor',true);
            //localStorage.setItem('admin',false);
            //console.log(localStorage.getItem('ventas')==='true' && localStorage.getItem('ventas')==='false')
            if(localStorage.getItem('admin')==='true'){
                return (
                    <div>
                        <HomeAdmin>

                        </HomeAdmin>
                        <LogOut>

                        </LogOut>
                    </div>
                )
            }
            else if(localStorage.getItem('vendedor')==='true' && localStorage.getItem('bodeguero')==='true'){
                return (
                    <div>
                        <HomeVen>

                        </HomeVen>
                        <HomeBod>

                        </HomeBod>
                        <LogOut>
                            
                        </LogOut>
                    </div>
                )
            }
            else if(localStorage.getItem('vendedor')==='true'){
                return (
                    <div>
                        <HomeVen>

                        </HomeVen>
                        <LogOut>
                            
                        </LogOut>
                    </div>
                )
            }
            else if(localStorage.getItem('bodeguero')==='true'){
                return (
                    <div>
                        <HomeBod>

                        </HomeBod>
                        <LogOut>
                            
                        </LogOut>
                    </div>
                )
            }
        }
        return (
            <div >
                <FormGroup>
                    <h3>Log in</h3>

                    <div className="form-group">
                        <label>Correo electrónico</label>
                        <input type="email" className="form-control" name="correo" placeholder="Correo" onChange={this.handleChange} />
                    </div>

                    <div className="form-group">
                        <label>Contraseña</label>
                        <input type="password" className="form-control" name="contraseña" placeholder="Contraseña" onChange={this.handleChange} />
                    </div>
                    <button type="submit" onClick={()=>this.login()} className="btn btn-primary btn-block">Ingresar</button>
                </FormGroup>
            </div>
        )
        
    }
}

