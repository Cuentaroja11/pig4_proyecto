import React, { Component } from 'react'
import { FormGroup } from 'reactstrap';
import LogIn from '../Login/Login';


export default class Logout extends Component {
    
    logout(){
        localStorage.setItem('loggeado',false);
        localStorage.setItem('vendedor',false);
        localStorage.setItem('bodeguero',false);
        localStorage.setItem('repartidor',false);
        localStorage.setItem('admin',false); 
        window.location.reload();   
    }    



    render() {
        if (localStorage.getItem('loggeado')==='false'){
            return (
                <div>
                    <LogIn>

                    </LogIn>
                </div>
            )
        }
        return (
            <div >
                <FormGroup>
                    <h3>Log out</h3>
                    <button type="submit" onClick={()=>this.logout()} className="btn btn-primary btn-block">Cerrar Sesion</button>
                </FormGroup>
            </div>
        )
        
    }
}

