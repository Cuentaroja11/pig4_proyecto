import React, { Component, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';


let linkbackend="http://127.0.0.1:4000/";
let vez=1;

export default class Bodega extends React.Component {


    //getBodegas();
    state = {
        data: [],
        encargados: [],
        sedes: [],
        form: {
            codigobodega:'',
            nombre: '',
            direccion: '',
            estado: '',
            codigousuario: '',
            codigosede: ''
        },
        modalInsertar: false,
        modalEditar: false,
        dropdownOpen: false

    }

    getBodegas = () => {
        const url=linkbackend+"bodega";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ data: data }));
        console.log(this.state.data);

    }
    getEncargados = () => {
        const url=linkbackend+"getUsuarios";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ encargados: data }));
       // console.log(this.state.data);
        

    }

    getSedes = () => {
        const url=linkbackend+"getsedes";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ sedes: data }));
        console.log(this.state.data);

    }





    insertar = () => {
        const url=linkbackend+"bodega";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);
        var strjson = new String( JSON.stringify(valorNuevo));
       strjson= strjson.replace("CodigoUsuario", "codigousuario");
     console.log(strjson);
        fetch(url, {
            method: 'POST',
            body: strjson,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registor guardado!!");
                    this.getBodegas();
                }
               this.hideModalInsertar();
             
            })
    }


    editar = (Bodega) => {
        const url=linkbackend+"bodega";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);
        var strjson = new String( JSON.stringify(valorNuevo));
       strjson= strjson.replace("CodigoUsuario", "codigousuario");
     console.log(strjson);
        fetch(url, {
            method: 'PUT',
            body: strjson,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo actualizar el registro");
                }else{
                    alert("Registro actualizado!!");
                    this.getBodegas();
                }
               this.hideModalEditar();
             
            })
    }

    eliminar = (Bodega) => {
        var opcion=window.confirm("Desea eliminar el registro?");
        const url=linkbackend+"bodega/"+Bodega.codigobodega;
    
        if(opcion){
            fetch(url, {
                method: 'DELETE',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }
              })
                .then(res => res.json())
                .then(data => {
                    if(data.status==false){
                        alert("No se pudo eliminar el registro");
                    }else{
                        alert("Registor eliminado!!");
                    }
                   
          
                    
                    this.getBodegas();
                });
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true, form: registro });
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }



    getestado = (estado) => {
       if(estado=='a'){
        return "Activo";
       }else{
        return "Inactivo";
       }
    }

    render() {
        if(vez==1){
            this.getSedes();
            this.getEncargados();
            this.getBodegas();
            vez=2;
        }
     
        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success" onClick={() => this.showModalInsertar()}>
                            Agregar nueva Bodega
                     </Button>

                    </nav>
                    <br />

                    <Table dark>
                        <thead>

                        </thead>

                        <th>  nombre Bodega </th>
                        <th>  direccion</th>
                        <th>  estado</th>
                        <th>  opciones</th>


                        <tbody>
                            {this.state.data.map((Bodega) => (
                                <tr>

                                    <td> {Bodega.nombre}</td>
                                    <td> {Bodega.direccion}</td>
                                    <td> {this.getestado(Bodega.estado)}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(Bodega)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar(Bodega)}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Bodega</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                nombre Bodega:
                             </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                direccion:
                            </label>
                            <input
                                className="form-control"
                                name="direccion"
                                type="text"
                                onChange={this.handleChange}
                            />

                        </FormGroup>
                        <FormGroup >
                            <label>
                                estado:
                            </label>
                            <select className="form-control" name="estado" onChange={this.handleChange} >
                            <option value={-1}> </option>
                                <option value='a'>Activo</option>
                                <option value='i' >Inactivo</option>

                            </select>
                        </FormGroup>


                        <FormGroup>
                            <label>
                                Encargado:
                            </label>
                            <select className="form-control" name="codigousuario" onChange={this.handleChange}>
                            <option value={-1}> </option>
                                {this.state.encargados.map((encargado) => (
                                    <option value={encargado.Codigousuario}>{encargado.nombre}</option>

                                ))}

                            </select>



                        </FormGroup>
                        <FormGroup>
                            <label>
                                Sede:
                            </label>
                            <select className="form-control" name="codigosede" onChange={this.handleChange}>
                            <option value={-1}> </option>
                                {this.state.sedes.map((sede) => (
                                    <option value={sede.codigosede}>{sede.aliassede}</option>

                                ))}

                            </select>

                        </FormGroup>


                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Bodega</h3></div>
                    </ModalHeader>

                   
                    <ModalBody>

                        <FormGroup>
                            <label>
                                nombre Bodega:
                             </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                value={this.state.form.nombre}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                direccion:
                            </label>
                            <input
                                className="form-control"
                                name="direccion"
                                type="text"
                                value={this.state.form.direccion}
                                onChange={this.handleChange}
                            />

                        </FormGroup>
                        <FormGroup controlId="exampleForm.SelectCustom">
                            <label>
                                estado:
                            </label>
                            <select className="form-control"  value={this.state.form.estado} name="estado" onChange={this.handleChange}>
                   
                                <option value='a'>Activo</option>
                                <option value='i' >Inactivo</option>

                            </select>
                        </FormGroup>


                        <FormGroup>
                            <label>
                                Encargado:
                            </label>
                            <select className="form-control"  value={this.state.form.codigousuario} name="codigousuario" onChange={this.handleChange}>
                  
                                {this.state.encargados.map((encargado) => (
                                    <option value={encargado.Codigousuario}>{encargado.nombre}</option>
                                ))}

                            </select>
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Sede:
                            </label>
                            <select className="form-control" name="codigosede" onChange={this.handleChange} >
                       
                                {this.state.sedes.map((sede) => (
                                    <option value={sede.codigosede}>{sede.aliassede}</option>

                                ))}

                            </select>

                        </FormGroup>


                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

