import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';

let data = [];

let vez=1;
let linkbackend="http://127.0.0.1:4000/";
export default class Rol extends React.Component {
 
    state = {
        data:[],
        rolesus:[],
        form: {
            CodigoUsuario:this.props.id,
            CodigoRol: ''
        },
        modalInsertar: false,
        modalEditar: false
    }

    getRols = () => {
        const url=linkbackend+"getroles";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ data: data }));
       // console.log(this.state.data);

    }

    getRolsus = () => {
        const url=linkbackend+"rolesasignados/"+this.props.id;
        console.log(url);
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ rolesus: data }));
       // console.log(this.state.data);

    }





    insertar = () => {
        const url=linkbackend+"asignarrol";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo asignar el registro");
                }else{
                    alert("Registor asignado!!");
                    this.getRolsus();
                }
               this.hideModalInsertar();
   
            })
  
    }


    eliminar = (Rol) => {
        var opcion = window.confirm("Desea eliminar el Rol?");
        if (opcion) {
            //eliminar
        }
    }

    getnombrerol = (Rol) => {
        this.state.data.forEach(element => 
            {   
                console.log(Rol);
                console.log(element.codigorol);
                if(element.codigorol==Rol){
                    console.log(element.nombrerol);
                    return element.nombrerol;
                }
             
            });
    }


    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
        
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    render() {
     
   
            this.getRols();
            this.getRolsus();
       
        
      
 
        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                    <Button color="success"  onClick={() => this.showModalInsertar()}>
                            Agregar un rol al usuario {this.props.nombre}
                     </Button>
               
                    </nav>
                    <br />
                    <Table dark>
                        <thead>

                        </thead>

                        <th>  Roles Asignados </th>
                        <th>  Opciones </th>

                        <tbody>
                            {this.state.rolesus.map((Rol) => (
                                <tr>
                                    <td> {Rol.CodigoRol }</td>
                                    <td> <Button color="danger" onClick={() => this.eliminar()}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Rol</h3></div>
                    </ModalHeader>

                    <ModalBody>

                    <FormGroup>
                            <label>
                                Roles:
                            </label>
                            <select className="form-control"  onChange={this.handleChange} name="CodigoRol">
                            <option value={-1}> </option>
                                {this.state.data.map((rol) => (
                                    <option value={rol.codigorol}>{rol.nombrerol}</option>
                                ))}

                            </select>
                        </FormGroup>

                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Asignar Rol
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

