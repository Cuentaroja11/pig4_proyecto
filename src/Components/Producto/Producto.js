import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';
import Categoria from '../Categoria/Categoria';
import AsignacionCat from '../AsigCat/AsignacionCategoria';



let linkbackend="http://127.0.0.1:4000/";
let vez=1;
export default class Producto extends React.Component {
    //getProductos();
    state = {
        data: [],
        categorias:[],
        form: {
            codigoproducto:'',
            sku: '',
            codigobarras: '',
            nombreproducto: '',
            descripcion: '',
            precio: ''
        },
        modalInsertar: false,
        modalEditar: false,
        modalAsignacion:false
    }

    getProductos = () => {
        const url=linkbackend+"getproductos";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ data: data }));
        console.log(this.state.data);

    }




    insertar = () => {
        const url=linkbackend+"insertarproducto";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registor guardado!!");
                }
               this.hideModalInsertar();
              this.getProductos();
            })
  
    }


    editar = (Producto) => {
        const url=linkbackend+"actualizarproducto";
        var valorNuevo= {...this.state.form};
        console.log(JSON.stringify(valorNuevo));

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registor guardado!!");
                }
               this.hideModalInsertar();
              this.getProductos();
            })

    }

    eliminar = (Producto) => {
        var opcion=window.confirm("Desea eliminar el registro?");
        const url=linkbackend+"borrarproducto/"+Producto.codigoproducto;
    
        if(opcion){
            fetch(url, {
                method: 'DELETE',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }
              })
                .then(res => res.json())
                .then(data => {
                    if(data.status==false){
                        alert("No se pudo eliminar el registro");
                    }else{
                        alert("Registor eliminado!!");
                    }
                   
          
                    
                    this.getProductos();
                });
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true, form: registro });
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }

    showModalAsignacion = () => {
        this.hideModalEditar();
        this.setState({ modalAsignacion: true});
    }

    hideModalAsignacion = () => {
        this.setState({ modalAsignacion: false });
    }

    render() {
        if(vez==1){
            this.getProductos();
            vez=2;
        }

        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success" onClick={() => this.showModalInsertar()}>
                            Agregar nuevo Producto
                     </Button>

                    </nav>
                    <br />
                    <Table dark>
                        <thead>

                        </thead>
                        <th>  sku </th>
                        <th>  Codigo de Barras </th>
                        <th>  Nombre Producto </th>
                        <th>  Descripcion Producto</th>
                        <th>  Precio</th>
                        <th>  Opciones</th>

                        <tbody>
                            {this.state.data.map((Producto) => (
                                <tr>
                                    <td> {Producto.sku}</td>
                                    <td> {Producto.codigobarras}</td>
                                    <td> {Producto.nombreproducto}</td>
                                    <td> {Producto.descripcion}</td>
                                    <td> {Producto.precio}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(Producto)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar(Producto)}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Producto</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                sku:
                             </label>
                            <input
                                className="form-control"
                                name="sku"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Codigo Barras:
                             </label>
                            <input
                                className="form-control"
                                name="codigobarras"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Nombre Producto:
                             </label>
                            <input
                                className="form-control"
                                name="nombreproducto"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Descripcin Producto:
                            </label>
                            <input
                                className="form-control"
                                name="descripcion"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Precio:
                            </label>
                            <input
                                className="form-control"
                                name="precio"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        
                        
           

                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Producto</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                sku:
                                 </label>
                            <input
                                className="form-control"
                                name="sku"
                                type="text"
                                value={this.state.form.sku}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Codigo Barras:
                                 </label>
                            <input
                                className="form-control"
                                name="codigobarras"
                                value={this.state.form.codigobarras}
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Nombre Producto:
                                 </label>
                            <input
                                className="form-control"
                                name="nombreproducto"
                                value={this.state.form.nombreproducto}
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Descripcin Producto:
                                </label>
                            <input
                                className="form-control"
                                name="descripcion"
                                type="text"
                                value={this.state.form.descripcion}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Precio:
                                </label>
                            <input
                                className="form-control"
                                name="precio"
                                type="text"
                                value={this.state.form.precio}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

               



                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>
                        <Button color="primary" onClick={() => this.showModalAsignacion()}>
                            Agregar Categorias
                        </Button>
                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                 
                <Modal isOpen={this.state.modalAsignacion}>
                    <ModalHeader>
                        <div><h3>Agregar Categorias al Producto</h3></div>
                    </ModalHeader>

                    <ModalBody>
                     <AsignacionCat nombre={this.state.form.nombreproducto} id={this.state.form.codigoproducto}>

                     </AsignacionCat>

                    </ModalBody>
                    <ModalFooter>
              

                        <Button className="btn btn-danger" onClick={() => this.hideModalAsignacion()}>
                            Cerrar
                         </Button>
                    </ModalFooter>

                </Modal>
            </div>
        );
    }
}

