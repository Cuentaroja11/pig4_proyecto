import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter, Toast } from 'reactstrap';
import { Link } from 'react-router-dom';
import Rol from '../Rol/Rol';

//let datos =[];// [{ CodigoUsuario:1, dpi: "3434", nombre: "juan",contraseña:"12345", fechanac: "Fecha", correo: "asdfas@gmail.com", roles: "1,2" }];

let roles = [{ CodigoRol: 1, NombreRol: "Vendedor"},{ CodigoRol: 2, NombreRol: "Bodeguero"}];
 let vez=1;
let linkbackend="http://127.0.0.1:4000/";
export default class Usuario extends React.Component {
    state = {
        data: [],
        usr:{
            CodigoUsuario:'',
            dpi: '',
            nombre: '',
            contraseña:'',
            fechanac: '',
            correo: '',
            roles: ''
        },
        form: {
            CodigoUsuario:'',
            dpi: '',
            nombre: '',
            contraseña:'',
            fechanac: '',
            correo: '',
            roles: ''
        },
        roles:false,
        modalInsertar: false,
        modalEditar: false
    }

    getusuarios=()=>{
         const url=linkbackend+"getUsuarios";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ data: data }));
       // console.log(this.state.data);
        
    }
    




    insertar= ()=>{
        const url=linkbackend+"user";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registro guardado!!");
                }
               this.hideModalInsertar();
              this.getusuarios();
            })
  
      }

      
    editar= (usuario)=>{
      
        const url=linkbackend+"updateUser";
        var valorNuevo= {...this.state.form};
        console.log(JSON.stringify(valorNuevo));

        fetch(url, {
            method: 'PUT',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registro guardado!!");
                }
               this.hideModalInsertar();
              this.getusuarios();
            })
    }

    eliminar= (usuario)=>{
        var opcion=window.confirm("Desea eliminar el usuario?");
        const url=linkbackend+"deleteUser";
        console.log(usuario);
        const json="{ \"CodigoUsuario\":"+usuario.Codigousuario+"}";
        console.log(json);
        if(opcion){
            fetch(url, {
                method: 'DELETE',
                body: json,
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }
              })
                .then(res => res.json())
                .then(data => {
                    if(data.status==false){
                        alert("No se pudo eliminar el registro");
                    }else{
                        alert("Registro eliminado!!");
                    }
                   
          
                    
                    this.getusuarios();
                });
        }
      }

      

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });




    }

    showaddRoles=(usuario)=>{
        this.hideModalEditar();
        this.setState({ roles: true,usr:usuario});
    }
    hideModalroles=(usuario)=>{
        this.setState({ roles: false });
    }
    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true,form:registro});
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }




    render() {
        if(vez==1){
            this.getusuarios();
            vez=2;
        }
       

        return (

            <div>
                <Container>
                <nav aria-label="breadcrumb">

                        <Button color="success"  onClick={() => this.showModalInsertar()}>
                            Agregar nuevo usuario
                     </Button>
               
                    </nav>
                    <br />
                
                    <Table dark>
                        <thead>

                        </thead>
                        <th>  DPI</th>
                        <th>  Nombre </th>
                        <th>  FechaNac</th>
                        <th>  Correo</th>
                        <th> Opciones </th>
                        <tbody>
                            {this.state.data.map((usuario) => (
                                <tr>
                                    <td> {usuario.dpi}  </td>
                                    <td> {usuario.nombre}</td>
                                    <td> {usuario.fechanac}</td>
                                    <td> {usuario.correo}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(usuario)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar(usuario)}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Insertar Usuario</h3></div>
                    </ModalHeader>

                    <ModalBody>
                        <FormGroup>
                            <label>
                                DPI:
                            </label>

                            <input
                                className="form-control"
                                name="dpi"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Nombre:
                             </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Contraseña:
                             </label>
                            <input
                                className="form-control"
                                name="contraseña"
                                type="password"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                 

                        <FormGroup>
                            <label>
                                Fecha Nacimiento:
                            </label>
                            <input
                                className="form-control"
                                name="fechanac"
                                type="date"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Correo:
                             </label>
                            <input
                                className="form-control"
                                name="correo"
                                type="email"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

            

                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Usuario</h3></div>
                    </ModalHeader>

                    <ModalBody>
                        <FormGroup>
                            <label>
                                DPI:
                            </label>

                            <input
                                className="form-control"
                                name="dpi"
                                type="text"
                                onChange={this.handleChange}
                                value={this.state.form.dpi}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Nombre:
                             </label>
                            <input
                                className="form-control"
                                name="nombre"
                                type="text"
                                onChange={this.handleChange}
                                value={this.state.form.nombre}
                            />
                        </FormGroup>


                        <FormGroup>
                            <label>
                                Contraseña:
                             </label>
                            <input
                                className="form-control"
                                name="contraseña"
                                type="password"
                                value={this.state.form.contraseña}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Fecha Nacimiento:
                            </label>
                            <input
                                className="form-control"
                                name="fechanac"
                                type="date"
                                onChange={this.handleChange}
                                value={this.state.form.fechanac}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Correo:
                             </label>
                            <input
                                className="form-control"
                                name="correo"
                                type="email"
                                onChange={this.handleChange}
                                value={this.state.form.correo}
                            />
                        </FormGroup>

                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>
                        <Button color="primary" onClick={() => this.showaddRoles()}>
                            Agregar Roles
                        </Button>


                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>



                
                <Modal isOpen={this.state.roles}>
                    <ModalHeader>
                        <div><h3>Agregar Roles a Usuario</h3></div>
                    </ModalHeader>

                    <ModalBody>
                     <Rol nombre={this.state.form.nombre} id={this.state.form.Codigousuario}>

                     </Rol>

                    </ModalBody>
                    <ModalFooter>
              

                        <Button className="btn btn-danger" onClick={() => this.hideModalroles()}>
                            Cerrar
                         </Button>
                    </ModalFooter>

                </Modal>
            </div>
        );
    }
}

