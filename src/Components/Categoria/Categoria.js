import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';
let vez=1;
let linkbackend="http://127.0.0.1:4000/";
export default class Categoria extends React.Component {
    //getCategorias();
    state = {
        data: [],
        form: {
            nombrecategoria: '',
            descripcioncategoria: '',
        },
        modalInsertar: false,
        modalEditar: false
    }

    getCategorias = () => {
        const url=linkbackend+"categories";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ data: data }));
       // console.log(this.state.data);

    }




    insertar = () => {
        const url=linkbackend+"agregateCategory";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registor guardado!!");
                }
               this.hideModalInsertar();
              this.getCategorias();
            })
  
    }


    editar = (Categoria) => {
        const url=linkbackend+"updateCategory";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(valorNuevo),
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo actualizar el registro");
                }else{
                    alert("Registro actualizado!!");
                }
               this.hideModalEditar();
              this.getCategorias();
            })
    }

    eliminar = (Categoria) => {
        var opcion = window.confirm("Desea eliminar la Categoria?");
        const url=linkbackend+"deleteCategory";
        console.log(Categoria);
        const json="{ \"codigocategoria\":"+Categoria.codigocategoria+"}";
        console.log(json);
        if (opcion) {
            fetch(url, {
                method: 'DELETE',
                body: json,
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }
              })
                .then(res => res.json())
                .then(data => {
                    if(data.status==false){
                        alert("No se pudo eliminar el registro");
                    }else{
                        alert("Registor eliminado!!");
                    }
                   
          
                    
                    this.getCategorias();
                });
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true, form: registro });
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }
    render() {

        if(vez==1){
            this.getCategorias();
        }
        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success"  onClick={() => this.showModalInsertar()}>
                            Agregar nueva categoria
                     </Button>
               
                    </nav>
                    <br />
                    <Table dark>
                        <thead>

                        </thead>

                        <th>  Nombre Categoria </th>
                        <th>  Descripcion Categoria</th>
                        <th>  Opciones</th>

                        <tbody>
                            {this.state.data.map((Categoria) => (
                                <tr>

                                    <td> {Categoria.nombrecategoria}</td>
                                    <td> {Categoria.descripcioncategoria}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(Categoria)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar(Categoria)}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Categoria</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Nombre Categoria:
                             </label>
                            <input
                                className="form-control"
                                name="nombrecategoria"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Descripcin Categoria:
                            </label>
                            <input
                                className="form-control"
                                name="descripcioncategoria"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>


                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Categoria</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Nombre Categoria:
                         </label>
                            <input
                                className="form-control"
                                name="nombrecategoria"
                                type="text"
                                value={this.state.form.nombrecategoria}
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Descripcin Categoria:
                        </label>
                            <input
                                className="form-control"
                                name="descripcioncategoria"
                                type="text"
                                value={this.state.form.descripcioncategoria}
                                onChange={this.handleChange}
                            />
                        </FormGroup>


                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

