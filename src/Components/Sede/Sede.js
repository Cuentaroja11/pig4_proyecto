import React, { Component, useSate } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import { Link } from 'react-router-dom';


let linkbackend="http://127.0.0.1:4000/";
let vez=1;
export default class Sede extends React.Component {
    
    state = {
        data: [],
        users:[],
        form: {
            codigosede:'',
            aliassede: '',
            direccionsede: '',
            departamento: '',
            municipio:'',
            CodigoUsuario:''
        },
        modalInsertar: false,
        modalEditar: false
    }

    getSedes = () => {
        const url=linkbackend+"getsedes";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ data: data }));
        console.log(this.state.data);
    }

    getUsuarios = () => {
        const url=linkbackend+"getUsuarios";
        fetch(url)
        .then(response => response.json())
        .then(data => this.setState({ users: data }));
       // console.log(this.state.data);
        

    }





    insertar = () => {
        const url=linkbackend+"insertarsede";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);
        var strjson = new String( JSON.stringify(valorNuevo));
       strjson= strjson.replace("CodigoUsuario", "codigousuario");
     console.log(strjson);
        fetch(url, {
            method: 'POST',
            body: strjson,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo guardar el registro");
                }else{
                    alert("Registor guardado!!");
                    this.getSedes();
                }
               this.hideModalInsertar();
             
            })
    }


    editar = (Sede) => {
        const url=linkbackend+"actualizarsede";
        var valorNuevo= {...this.state.form};
        console.log(valorNuevo);
        var strjson = new String( JSON.stringify(valorNuevo));
       strjson= strjson.replace("CodigoUsuario", "codigousuario");
     console.log(strjson);
        fetch(url, {
            method: 'POST',
            body: strjson,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            }
          })
            .then(res => res.json())
            .then(data => {
                if(data.status==false){
                    alert("No se pudo actualizar el registro");
                }else{
                    alert("Registor actualizado!!");
                    this.getSedes();
                }
               this.hideModalEditar();
             
            })
    }

    eliminar = (Sede) => {
        var opcion=window.confirm("Desea eliminar el registro?");
        const url=linkbackend+"borrarsede/"+Sede.codigosede;
    
        if(opcion){
            fetch(url, {
                method: 'DELETE',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }
              })
                .then(res => res.json())
                .then(data => {
                    if(data.status==false){
                        alert("No se pudo eliminar el registro");
                    }else{
                        alert("Registor eliminado!!");
                    }
                   
          
                    
                    this.getSedes();
                });
        }
    }



    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
            }
        });
    }

    showModalInsertar = () => {
        this.setState({ modalInsertar: true });
    }

    hideModalInsertar = () => {
        this.setState({ modalInsertar: false });
    }

    showModalEditar = (registro) => {
        this.setState({ modalEditar: true, form: registro });
    }

    hideModalEditar = () => {
        this.setState({ modalEditar: false });
    }
    render() {
        if(vez==1){
            this.getSedes();
            this.getUsuarios();
            vez=2;
        }
     
        return (

            <div>
                <Container>

                    <nav aria-label="breadcrumb">

                        <Button color="success"  onClick={() => this.showModalInsertar()}>
                            Agregar nueva Sede
                     </Button>
               
                    </nav>
                    <br />
                    <Table dark>
                        <thead>

                        </thead>

                        <th>  Alias Sede </th>
                        <th>  Direccion Sede</th>
                        <th>  departamento</th>
                        <th>  municipio</th>
                        <th>  Opciones</th>
                        <tbody>
                            {this.state.data.map((Sede) => (
                                <tr>

                                    <td> {Sede.aliassede}</td>
                                    <td> {Sede.direccionsede}</td>
                                    <td> {Sede.departamento}</td>
                                    <td> {Sede.municipio}</td>
                                    <td><Button color="primary" onClick={() => this.showModalEditar(Sede)}>Editar </Button>
                                        {" "}
                                        <Button color="danger" onClick={() => this.eliminar(Sede)}>Eliminar </Button> </td>

                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div><h3>Agregar Sede</h3></div>
                    </ModalHeader>

                    <ModalBody>

                        <FormGroup>
                            <label>
                                Alias Sede:
                             </label>
                            <input
                                className="form-control"
                                name="aliassede"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Direccion Sede:
                            </label>
                            <input
                                className="form-control"
                                name="direccionsede"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                Ddepartamento:
                            </label>
                            <input
                                className="form-control"
                                name="departamento"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                municipio:
                            </label>
                            <input
                                className="form-control"
                                name="municipio"
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                Encargado:
                            </label>
                            <select className="form-control"  onChange={this.handleChange} name="CodigoUsuario">
                            <option value={-1}> </option>
                                {this.state.users.map((user) => (
                                    <option value={user.Codigousuario}>{user.nombre}</option>
                                ))}

                            </select>
                        </FormGroup>

                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" onClick={() => this.insertar()}>
                            Insertar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalInsertar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div><h3>Editar Sede</h3></div>
                    </ModalHeader>

                
                    <ModalBody>

                        <FormGroup>
                            <label>
                                Alias Sede:
                             </label>
                            <input
                                className="form-control"
                                name="aliassede"
                                value={this.state.form.aliassede}
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>

                        <FormGroup>
                            <label>
                                Direccion Sede:
                            </label>
                            <input
                                className="form-control"
                                name="direccionsede"
                                value={this.state.form.direccionsede}
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                Ddepartamento:
                            </label>
                            <input
                                className="form-control"
                                name="departamento"
                                value={this.state.form.departamento}
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                municipio:
                            </label>
                            <input
                                className="form-control"
                                name="municipio"
                                value={this.state.form.municipio}
                                type="text"
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>
                                Encargado:
                            </label>
                            <select className="form-control"  onChange={this.handleChange} name="CodigoUsuario">
                            <option value={-1}> </option>
                                {this.state.users.map((user) => (
                                    <option value={user.Codigousuario}>{user.nombre}</option>
                                ))}

                            </select>
                        </FormGroup>

                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={() => this.editar()}>
                            Editar
                        </Button>

                        <Button className="btn btn-danger" onClick={() => this.hideModalEditar()}>
                            Cancelar
                         </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

